module .kapigen

go 1.22.1

require (
	gitlab.com/kateops/kapigen/cli v0.0.0-20241225110133-9a85f5dd44ff
	gitlab.com/kateops/kapigen/dsl v0.0.0-20241222185954-285d42dcba54
)

require (
	github.com/Masterminds/semver/v3 v3.2.1 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.7 // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/xanzy/go-gitlab v0.107.0 // indirect
	golang.org/x/oauth2 v0.22.0 // indirect
	golang.org/x/time v0.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
