package main

import (
	"gitlab.com/kateops/kapigen/cli/pipelines"
	"gitlab.com/kateops/kapigen/dsl/enum"
	"gitlab.com/kateops/kapigen/dsl/gitlab/job"
	"gitlab.com/kateops/kapigen/dsl/gitlab/pipeline"
	"gitlab.com/kateops/kapigen/dsl/gitlab/stages"
	"gitlab.com/kateops/kapigen/dsl/logger"
	"gitlab.com/kateops/kapigen/dsl/types"
)

func main() {
	pipelines.CreatePipeline(func(jobs *types.Jobs, mainPipeline *pipeline.CiPipeline) {
		// pipeline config
		mainPipeline.DefaultCiPipeline()
		//os.Setenv("LOGGER_LEVEL", "DEBUG")
		logger.DebugAny(mainPipeline)

		// test job config
		testJob := types.NewJob("test", "alpine", func(ciJob *job.CiJob) {
			logger.Info("test")
			// jobs config
			ciJob.AddScript("echo 'hello world'").
				SetStage(stages.TEST).
				Rules.AddRules(*job.DefaultPipelineRules([]string{"."}))
			ciJob.Tags.Add(enum.TagDefaultRunner)

		})

		// need job config
		needJob := types.NewJob("need", "alpine", func(ciJob *job.CiJob) {
			ciJob.AddScript("echo 'waiting for test'").
				SetStage(stages.TEST).
				Rules.AddRules(*job.DefaultPipelineRules([]string{"."}))
			ciJob.Tags.Add(enum.TagDefaultRunner)
		})

		// set dependency between jobs
		testJob.AddJobAsNeed(needJob)
		jobs.
			AddJob(testJob).
			AddJob(needJob)
	})
}
